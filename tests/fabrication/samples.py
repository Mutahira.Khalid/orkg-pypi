from typing import Any, Dict

from faker import Faker

faker = Faker()


class SamplesFabricator:
    @staticmethod
    def paper_sample() -> Dict[str, Any]:
        return {
            "predicates": [],
            "paper": {
                "title": "Paper title",
                "doi": "10.1145/3360901.3364435",
                "authors": [
                    {"label": faker.name()},
                ],
                "publicationMonth": "",
                "publicationYear": 2019,
                "publishedIn": "Venue name",
                "researchField": "R11",
                "contributions": [
                    {
                        "name": "Contribution 1",
                        "values": {
                            "P32": [
                                {
                                    "@temp": "_5254e420-ae9a-13ef-1c18-e716b9ea5c2b",
                                    "label": faker.name(),
                                    "class": "Problem",
                                    "values": {},
                                }
                            ],
                            "P1": [
                                {
                                    "@temp": "_851da0a6-7e83-c276-8df3-f66d0a680b30",
                                    "label": faker.word(),
                                    "values": {
                                        "P2": [
                                            {
                                                "text": faker.sentence(),
                                            }
                                        ]
                                    },
                                }
                            ],
                        },
                    }
                ],
            },
        }

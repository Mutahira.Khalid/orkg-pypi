from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestLiterals(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.literals.by_id, return_value=Fabricator.literals.success_by_id("L1")
    )
    def test_by_id(self, *args):
        res = self.orkg.literals.by_id("L1")
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.literals.get_all, return_value=Fabricator.literals.success_get_all()
    )
    def test_get(self, *args):
        res = self.orkg.literals.get_all()
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.literals.get_all,
        return_value=Fabricator.literals.success_get_all_by_term(q="Python"),
    )
    def test_get_term(self, *args):
        res = self.orkg.literals.get_all(q="Python")
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.literals.add,
        return_value=Fabricator.literals.success_add(label="ORKG rocks"),
    )
    def test_add(self, *args):
        label = "ORKG rocks"
        res = self.orkg.literals.add(label=label)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)

    @mock_test(
        orkg.literals.add,
        return_value=Fabricator.literals.success_add(id="Updatable", label="ORKG"),
    )
    @mock_test(
        orkg.literals.update,
        return_value=Fabricator.literals.success_update(
            id="Updatable", label="Open Research Knowledge Graph"
        ),
    )
    @mock_test(
        orkg.literals.by_id,
        return_value=Fabricator.literals.success_by_id_from_cache(_id="Updatable"),
    )
    def test_update(self, *args):
        res = self.orkg.literals.add(label="ORKG")
        self.assertTrue(res.succeeded)
        label = "Open Research Knowledge Graph"
        res = self.orkg.literals.update(id=res.content["id"], label=label)
        self.assertTrue(res.succeeded)
        res = self.orkg.literals.by_id(res.content["id"])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)

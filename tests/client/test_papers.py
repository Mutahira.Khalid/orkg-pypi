from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestPapers(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.papers.add, return_value=Fabricator.success())
    def test_add(self, *args):
        paper = Fabricator.samples.paper_sample()
        res = self.orkg.papers.add(params=paper)
        self.assertTrue(res.succeeded)

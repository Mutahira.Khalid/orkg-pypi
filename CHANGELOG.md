# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [[0.18.0]](https://pypi.org/project/orkg/0.18.0/) - 2023-08-29
### Added
- Add changelog ([#35](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/35)).
- Ping host functionality `orkg.ping()` ([#36](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/36)).
- Cast `OrkgResource` to `Type` ([#41](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/41)).
- Type client property of `NamespacedClient` ([#49](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/49)).
- Add directory harvesting for template JSON-LD output ([#53](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/53)).

### Fixed
- Decimal datatype is supported as DF within templates ([#42](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/42)).
- Improve backend client tests ([#37](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/37)).
- Fix IDE typing hints for `ORKG` base class ([!46](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/46)).
- Simplify code for `add_csv` and `compare_dataframe` functions ([#34](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/34)).
- Importing tables via templated instances was empty ([be97df05](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/commit/be97df05b2bf79cdd66708ec6af9f54c025198b6#5b8b17dd581083e42da539995a460397ab07cbb5_0_60)).

## [[0.17.0]](https://pypi.org/project/orkg/0.17.0/) - 2023-08-03

### Added
- Mock tests ([#28](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/28)).
- Improve host and simcomp host assignment ([#33](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/33)).
- Add [contribution guidelines](CONTRIBUTING.md).

### Changed
- Switch minimum python version to 3.8.
- Default host of the package set to Sandbox.

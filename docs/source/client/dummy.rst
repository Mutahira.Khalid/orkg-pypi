ORKG Dummy client
=================
To create dummy `OrkgResponse` objects, this dummy client provides the following functionality

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the dummy data manager directly to do the following:

Creating a 200 response
^^^^^^^^^^^^^^^^^^^^^^^
With the following line of code you can easily create a dummy response.

.. code-block:: python

    ### all the parameters are required
    orkg.dummy.create_200_response(content={'orkg': 'super easy'})
    >>> (Success)
    {
       "orkg":"super easy"
    }

Creating a 404 response
^^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup literals by the value of the literal.

.. code-block:: python

    ### all the parameters are required
    orkg.dummy.create_404_response(content=[{'msg': 'bad content'}])
    >>> (Fail)
    [
       {
          "msg":"bad content"
       },
       ...
    ]

More generic responses
^^^^^^^^^^^^^^^^^^^^^^
You can create any XXX HTTP status code response to match your testing or mocking needs via the following function:

.. code-block:: python

    ### all the parameters are required
    orkg.dummy.create_xxx_response(code='500', content={'error': 'server died'})
    >>> (Fail)
    {
       "error":"server died"
    }

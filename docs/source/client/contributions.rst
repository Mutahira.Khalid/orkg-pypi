ORKG Contributions client
=========================
The contribution client is a special client of the ORKG that handles contributions and comparisons. For that reason it needs to be connected to the `simcomp` service of the open research knowledge graph.

If you instantiate the ORKG client using a web address or host environment name (``Hosts.PRODUCTION``, ``Hosts.SANDBOX``, or ``Hosts.INCUBATING``), the corresponding `simcomp_host` is automatically assigned.

However, if you are running the ORKG locally then you need to specify `simcomp_host`. See :ref:`Host <host-label>` for details.

We can access the contributions client directly to do the following:


Get similar contributions
^^^^^^^^^^^^^^^^^^^^^^^^^
With the following line of code you can easily create a dummy response.

.. code-block:: python

    ### all the parameters are required
    orkg.contributions.similar(cont_id='R8197')
    >>> (Success)
    [
       {
          "contributionId":"R6142",
          "contributionLabel":"Contribution 1",
          "paperId":"R6141",
          "similarityPercentage":0.13443964521673085
       },
       {
          "contributionId":"R8199",
          "contributionLabel":"ORKG System",
          "paperId":"R8186",
          "similarityPercentage":0.1338162327194794
       }
    ]

Comparing several contributions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can perform a comparison of several contributions easily as well.

.. code-block:: python

    orkg.contributions.compare(contributions=['R6151','R6173','R6162','R6179','R6157','R6146'])
    >>> (Success)
    {
       "contributions":[
          {
             "contributionLabel":"Contribution 1",
             "id":"R6151",
             "paperId":"R6150",
             "title":"Author name disambiguation using a graph model with node splitting and merging based on bibliographic information",
             "year":"2014"
          },
          ....
       ],
       "data":{
          "METHOD":[
             [
                {
                   "label":"Graph Node Splitting & Merging",
                   "path":[
                      "R6151",
                      "METHOD"
                   ],
                   "pathLabels":[
                      "Contribution 1",
                      "Method"
                   ],
                   "resourceId":"L4214",
                   "type":"literal"
                }
             ],
             [
                {
                   "label":"Pairwise Factor Graph",
                   "path":[
                      "R6173",
                      "METHOD"
                   ],
                   "pathLabels":[
                      "Contribution 1",
                      "Method"
                   ],
                   "resourceId":"R6163",
                   "type":"resource"
                },
                {
                   "label":"interaction of user to enhance the performance",
                   "path":[
                      "R6173",
                      "METHOD"
                   ],
                   "pathLabels":[
                      "Contribution 1",
                      "Method"
                   ],
                   "resourceId":"L4239",
                   "type":"literal"
                }
             ],
          ]
       },
       "properties":[
          {
             "active":true,
             "contributionAmount":6,
             "id":"P5001",
             "label":"deals with"
          },
          {
             "active":true,
             "contributionAmount":6,
             "id":"P5004",
             "label":"Evidence"
          },
          ...
       ]
    }

    # To make use of the saved/cached version of the comparison, you can provide the response hash
    # This will be much faster because it the comparison is not done on the fly rather fetched from the database.
    orkg.contributions.compare(contributions=['R6151','R6173','R6162','R6179','R6157','R6146'], response_hash='972c8cef92829c1fa5c3b43caf5709c7')
    >>> (Success)
    {
        ...
    }
    ## Same response as before

Comparisons as pandas.DataFrame
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To make it easy to process the content of the comparison and not worry about the JSON format that is returned from the backend.

You can easily deal with the data as pandas.DataFrame object that you can use in down-stream applications.

.. warning::

    The python package tries to type the values inside the dataframe with pythonic types (float, int, etc..). Also when multiple values do belong to one cell, rather than splitting them with a '/' character now it is appended to a python list.

.. code-block:: python

    ### One of the parameters is required
    # Using the comparison ID
    df = orkg.contributions.compare_dataframe(comparison_id='R6751')
    type(df)
    >>> <pandas.core.frame.DataFrame>
    df.head()
    >>> ...

    # Using the list of contributions
    orkg.contributions.compare_dataframe(contributions=['R6151','R6173','R6162','R6179','R6157','R6146'])
    type(df)
    >>> <pandas.core.frame.DataFrame>
    df.head()
    >>> ...


The function also offers a parameter to decide whether you would like the dataframe to look exactly like the comparison on the ORKG UI (frontend), or not to get the complete and unfiltered comparison.

.. code-block:: python

    # param like_ui is True by default
    df = orkg.contributions.compare_dataframe(comparison_id='R6751', like_ui=True)
    df.head()
    >>> ...
    >>> [5 rows x 11 columns]

    df = orkg.contributions.compare_dataframe(comparison_id='R6751', like_ui=False)
    df.head()
    >>> ...
    >>> [12 rows x 11 columns]

There is an optional parameter which returns a second dataframe with the following paper metadata (where available): author, doi, publication month, publication year, url, research field, venue, title, paper id, and contribution id.

.. code-block:: python

    # param include_meta is False by default
    df, df_meta = orkg.contributions.compare_dataframe(contributions=['R34499', 'R34504'], include_meta=True)
    df.head()
    >>> ...
    >>> [5 rows x 2 columns]
    df_meta.head()
    >>> ...
    >>> [8 rows x 2 columns]

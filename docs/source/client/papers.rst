ORKG Papers
===========
One of the main power points of the ORKG is the ability to represent scientific papers in a structured manner. The package also provide an endpoint to add new papers directly using the concepts defined on other components.

A paper technically is a collection of resources, literals, and statements connecting them in a small sub-graph.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the papers manager directly to do the following:

Adding new papers
^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new papers in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added papers will be credited to your user.*

.. code-block:: python

    # First define the paper content as dict (JSON object) (This is a sample of input)
    paper = {
       "predicates":[

       ],
       "paper":{
          "title":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
          "doi":"10.1145/3360901.3364435",
          "authors":[
             {
                "label":"Mohamad Yaser Jaradeh"
             },
             {
                "label":"Allard Oelen"
             },
             {
                "label":"Kheir Eddine Farfar"
             },
             {
                "label":"Manuel Prinz"
             },
             {
                "label":"Jennifer D'Souza"
             },
             {
                "label":"Gábor Kismihók"
             },
             {
                "label":"Markus Stocker"
             },
             {
                "label":"Sören Auer"
             }
          ],
          "publicationMonth":"",
          "publicationYear":2019,
          "publishedIn":"Proceedings of the 10th International Conference on Knowledge Capture  - K-CAP '19",
          "researchField":"R11",
          "contributions":[
             {
                "name":"Contribution 1",
                "values":{
                   "P32":[
                      {
                         "@temp":"_5254e420-ae9a-13ef-1c18-e716b9ea5c2b",
                         "label":"Similarity measures",
                         "class":"Problem",
                         "values":{

                         }
                      }
                   ],
                   "P3":[
                      {
                         "text":"Find similar research contributions inside the ORKG and suggest them to the user"
                      }
                   ],
                   "P1":[
                      {
                         "@temp":"_851da0a6-7e83-c276-8df3-f66d0a680b30",
                         "label":"TF/iDF",
                         "values":{
                            "P2":[
                               {
                                  "text":"Term frequency, inverse document frequency technique"
                               }
                            ]
                         }
                      }
                   ]
                }
             }
          ]
       }
    }

    # Then add the content as a paper to the ORKG instance
    orkg.papers.add(params=paper)
    >>> (Success)
    {
       "id":"R8186",
       "label":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
       "created_at":"2019-12-19T15:07:02.136+01:00",
       "classes":[
          "Paper"
       ],
       "shared":0,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

    # you can append the contribution into the paper if it exists in the system by specifying the `merge_if_exists` property
    orkg.papers.add(params=paper, merge_if_exists=True)
    >>> (Success)
    {
       "id":"R8186",
       "label":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
       "created_at":"2019-12-19T15:07:02.136+01:00",
       "classes":[
          "Paper"
       ],
       "shared":0,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Adding new papers via CSV
^^^^^^^^^^^^^^^^^^^^^^^^^
An easier way to import papers from a csv file containing the details is detailed in this `Wiki page <https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/wikis/Tutorial:-importing-papers-in-CSV-format-into-the-ORKG>`_.

.. warning::
    This functionality is deprecated as of version 0.16.3 and will be removed in a future update! You can still add papers by CSV import using the frontend.
